#include <iostream>
#include <vector>
#include <string>
//#include <auto>

using namespace std;

int main()
{
	vector <char> fullName;
	string firstName = " ";
	auto lastName = firstName;
	
	cout << "Enter your first name: ";
	cin >> firstName;
	
	cout << "Enter your last name: ";
	cin >> lastName;
	
	for(char ch: firstName)
	{
		fullName.push_back(ch);
	}
	
	fullName.push_back(" ");
	
	for(char ch: lastName)
	{
		fullName.push_back(ch);
	}
	
	cout << "/nYour name is ";
	
	for(char ch: fullName)
	{
		cout << ch;
	}
	
	return 0;
}
